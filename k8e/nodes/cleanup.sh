#!/bin/bash

unmount_kube_volumes() {
    SRC="/var/lib/kubelet"
    MOUNTS=`mount | grep ${SRC} | awk '{print $3}'`

    echo "$MOUNTS" | while read LINE; do
            sudo umount -f $LINE
    done

    sudo rm -rf ${SRC}

}

docker_clean_up() {
    echo ""
    echo "========== [ CleanUP Node data ] ==========="
    echo ""

    sudo docker stop $(sudo docker ps -q)

    #remove all containers
    sudo docker rm --force $(sudo docker ps -a -q) 

    #remove all docker images
    sudo docker rmi --force $(sudo docker images -q)

    #remove all unused volumes
    sudo docker volume prune --force

    #k8s
    sudo service docker stop

    unmount_kube_volumes

    sudo rm -rf /etc/cni
    sudo rm -fr /var/lib/docker/
    sudo rm -rf /etc/kubernetes
    sudo rm -rf /opt/cni
    sudo rm -rf /var/lib/calico/
    sudo rm -rf /var/lib/cni/
    sudo rm -rf /var/lib/etcd/
    sudo rm -rf /var/lib/rancher/*
    sudo rm -rf /var/run/calico

    sudo service docker start
}

firewall_reset() {
    sudo service firewalld stop
           ##
   ## set default policies to let everything in
    sudo ip6tables --policy INPUT   ACCEPT;
    sudo ip6tables --policy OUTPUT  ACCEPT;
    sudo ip6tables --policy FORWARD ACCEPT;

   ##
   ## start fresh
    sudo ip6tables -Z; # zero counters
    sudo ip6tables -F; # flush (delete) rules
    sudo ip6tables -X; # delete all extra chains

# IPv4

   ## 
   ## set default policies to let everything in
    sudo iptables --policy INPUT   ACCEPT;
    sudo iptables --policy OUTPUT  ACCEPT;
    sudo iptables --policy FORWARD ACCEPT;

   ##
   ## start fresh
    sudo iptables -Z; # zero counters
    sudo iptables -F; # flush (delete) rules
    sudo iptables -X; # delete all extra chains

    sudo yum remove firewalld -y
    sudo rm -rf /etc/firewalld/zones
    sudo yum install firewalld -y

    echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<zone>
  <short>Public</short>
  <description>For use in public areas. You do not trust the other computers on networks to not harm your computer. Only selected incoming connections are accepted.</description>
  <service name="ssh"/>
  <service name="dhcpv6-client"/>
</zone>
" | sudo tee -a /etc/firewalld/zones/public.xml

    sudo service firewalld start
}

# 2)


# 3)
firewall_reset

# 4) 
docker_clean_up