#!/bin/bash

echo ""
echo "============= [ K8E Node setup ] =============="
echo ""
echo ""
sudo yum -y update

install_supported_docker() {
    curl https://releases.rancher.com/install-docker/17.03.sh > /tmp/docker.sh
    sh /tmp/docker.sh

    sudo usermod -aG docker ${USER}
}

install_kubectl() {
    # http://kubernetes.io/docs/user-guide/prereqs/
}

setup_firewall() {
    sudo service firewalld start

    sudo firewall-cmd --permanent --add-service=http
    sudo firewall-cmd --permanent --add-service=https

    sudo firewall-cmd --permanent --add-port=2379/tcp
    sudo firewall-cmd --permanent --add-port=2380/tcp
    sudo firewall-cmd --permanent --add-port=6443/tcp
    sudo firewall-cmd --permanent --add-port=8472/udp
    sudo firewall-cmd --permanent --add-port=10250/tcp

    sudo firewall-cmd --permanent --zone=trusted --add-interface=flannel.1
    # Your internal network
    sudo firewall-cmd --permanent --zone=trusted --add-source=10.100.100.0/24
    # POD cidr
    sudo firewall-cmd --permanent --zone=trusted --add-source=10.42.0.0/16
    # Service cidr
    sudo firewall-cmd --permanent --zone=trusted --add-source=10.43.0.0/16

    sudo firewall-cmd --reload
    
}

install_deps() {
    sudo yum -y install iscsi-initiator-utils 
}




#----------------------------------------------------
#           INSTALLATION CHECKLIST
#----------------------------------------------------

# 1) Set hostname
echo "Current hostname is: "`hostname`
echo -n "Set hostname: "
read node_hostname
sudo hostnamectl set-hostname ${node_hostname}

echo "New hostname is: "`hostname`

# 2) Install docker
install_supported_docker

# 3) Install required dependencies
install_deps

# 4) Open necessary ports
setup_firewall

# 5) Remove docker repo in order to avoid docker update problems
sudo mv /etc/yum.repos.d/docker-ce.repo /etc/yum.repos.d/docker-ce.repo.bak

echo ""
echo "======= [ K8E Node setup - FINISHED ] =========="
echo ""
echo ""
echo "  Next to do: "
echo "      Go to the Rancher master node UI and create cluster."
echo ""