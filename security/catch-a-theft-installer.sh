#!/bin/bash

SCRIPT_DEST=/usr/local/bin/grab-photo

echo ""
echo " >> Installing necessary components"
sudo dnf install ffmpeg
echo ""
echo " >> Installing 'Grab-Photo' script"
cat <<EOF > ${SCRIPT_DEST}
#!/bin/bash
ts=`date +%s`
ffmpeg -f video4linux2 -s vga -i /dev/video0 -vframes 3 /tmp/vid-$ts.%01d.jpg
notify-send -u critical "Invalid password was entered"
exit 0  #important - has to exit with status 0
EOF
chmod +x ${SCRIPT_DEST}

echo ""
echo " >> Done"
